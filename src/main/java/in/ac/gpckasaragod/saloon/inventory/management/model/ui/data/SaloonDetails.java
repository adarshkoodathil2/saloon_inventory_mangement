/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.saloon.inventory.management.model.ui.data;

import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class SaloonDetails {
    private Integer id;
    private String saloonRegno;
    private String saloonName;
    private String barberName;

    public SaloonDetails(Integer id, String saloonRegno, String saloonName, String barberName) {
        this.id = id;
        this.saloonRegno = saloonRegno;
        this.saloonName = saloonName;
        this.barberName = barberName;
    }
    @Override
    public String toString(){
        
        return saloonRegno+"-"+saloonName;
        
    }
    private static final Logger LOG = Logger.getLogger(SaloonDetails.class.getName());

    public Integer getId() {
        return id;
    }

    public String getSaloonRegno() {
        return saloonRegno;
    }

    public String getSaloonName() {
        return saloonName;
    }

    public String getBarberName() {
        return barberName;
    }

    public static Logger getLOG() {
        return LOG;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setSaloonRegno(String saloonRegno) {
        this.saloonRegno = saloonRegno;
    }

    public void setSaloonName(String saloonName) {
        this.saloonName = saloonName;
    }

    public void setBarberName(String barberName) {
        this.barberName = barberName;
    }
}
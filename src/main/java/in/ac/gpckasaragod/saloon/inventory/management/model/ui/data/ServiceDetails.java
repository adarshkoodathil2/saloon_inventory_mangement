/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.saloon.inventory.management.model.ui.data;

/**
 *
 * @author student
 */
public class ServiceDetails {
    private Integer id;
    private Integer saloonId;
    private String serviceName;
    private Double price;

    public ServiceDetails(Integer id, Integer saloonId, String serviceName, Double price) {
        this.id = id;
        this.saloonId = saloonId;
        this.serviceName = serviceName;
        this.price = price;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSaloonId() {
        return saloonId;
    }

    public void setSaloonId(Integer saloonId) {
        this.saloonId = saloonId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

   

   
}


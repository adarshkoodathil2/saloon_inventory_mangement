/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.saloon.inventory.management.service;

import in.ac.gpckasaragod.saloon.inventory.management.model.ui.data.SaloonDetails;
import java.util.List;

/**
 *
 * @author student
 */
public interface SaloonDetailService {
      public String saveSaloon(String saloonRegno,String saloonName,String barberName);
    public SaloonDetails readSaloon(Integer Id);
    public List<SaloonDetails> getAllSaloon();
    public String updateSaloon(Integer id,String saloonRegno,String saloonName,String barberName);
    public String deleteSaloon(Integer id) ;
}

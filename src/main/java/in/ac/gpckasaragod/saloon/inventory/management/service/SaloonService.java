/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.saloon.inventory.management.service;

import in.ac.gpckasaragod.saloon.inventory.management.model.ui.data.ServiceDetails;
import java.util.List;


/**
 *
 * @author student
 */
public interface SaloonService {
    public String saveService(Integer saloonId, String serviceName, String price);
    public ServiceDetails readService(Integer Id);
    public List<ServiceDetails>getAllServices();
    public String updateService(Integer id, Integer saloonId, String ServiceName, String price);
    public String deleteService(Integer id) ;

   
    
}

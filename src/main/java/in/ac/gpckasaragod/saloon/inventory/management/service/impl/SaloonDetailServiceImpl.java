/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.saloon.inventory.management.service.impl;

import in.ac.gpckasaragod.saloon.inventory.management.model.ui.data.SaloonDetails;
import in.ac.gpckasaragod.saloon.inventory.management.service.SaloonDetailService;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author student
 */
public class SaloonDetailServiceImpl extends ConnectionServiceImpl implements SaloonDetailService{

    @Override
    public String saveSaloon(String saloonRegno, String saloonName, String barberName) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO SALOON(SALOON_REGNO,SALOON_NAME,BARBER_NAME) VALUES " +"('"+saloonRegno+"','"+saloonName+"','"+barberName+"')";
            System.out.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status!=1){
                return "Saved Failed";
            }
            else{
                return "Saved Successfully";
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(SaloonServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            return "Saved Failed";
        }
    }

    @Override
    public SaloonDetails readSaloon(Integer Id) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
          SaloonDetails saloonData =null;
        try{
            Connection connection =getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM  SALOON WHERE ID="+Id;
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                int id = resultSet.getInt("ID");
                String saloonRegno = resultSet.getString("SALOON_REGNO");
                String saloonName = resultSet.getString("SALOON_NAME");
                String barberName= resultSet.getString("BARBER_NAME");
                saloonData = new SaloonDetails(id,saloonRegno,saloonName,barberName);
               
            }
            
            
        } catch (SQLException ex) {
            Logger.getLogger(SaloonServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            
        }
        return saloonData;
    }

    @Override
    public List<SaloonDetails> getAllSaloon() {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
          List<SaloonDetails> saloons = new ArrayList<>();
    try{
        Connection connection =getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM SALOON";
    ResultSet resultSet = statement.executeQuery(query);
    
     while(resultSet.next()){
        int id = resultSet.getInt("ID");
              
                String saloonRegno = resultSet.getString("SALOON_REGNO");
                String saloonName = resultSet.getString("SALOON_NAME");
                String barberName= resultSet.getString("BARBER_NAME");
                SaloonDetails saloonData = new SaloonDetails(id,saloonRegno,saloonName,barberName);
               saloons.add((SaloonDetails) saloonData);
     }
        
    }   catch (SQLException ex) {
            Logger.getLogger(SaloonServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return saloons;
        
       
    }

    @Override
    public String updateSaloon(Integer id, String saloonRegno, String saloonName, String barberName) {
       // throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
       try{
            Connection connection =getConnection();
            Statement statement = connection.createStatement();
            String query = "UPDATE SALOON SET SALOON_REGNO ='"+saloonRegno+"',SALOON_NAME ='"+saloonName+"',BARBER_NAME = '"+barberName+"' WHERE ID="+id;
            System.out.print(query);
            int update = statement.executeUpdate(query);
            if(update !=1)
            return "Update Failed";
            else
                return "Updated successfully";
            
    }   catch (SQLException ex) {
        return "Updated Failed";
            
        }
    }

    @Override
    public String deleteSaloon(Integer id) {
       // throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
       try{
            Connection connection =getConnection();
            String query = "DELETE FROM SALOON WHERE ID=?";
            PreparedStatement statement = connection.prepareCall(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if (delete !=1)
                return "Delete Failed";
            else
                return "Deleted Successfully";
            
            
        } catch (Exception ex) {
            ex.printStackTrace();
            return "Deleted Failed";
    }
    }
}

   
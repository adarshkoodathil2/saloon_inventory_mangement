/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.saloon.inventory.management.service.impl;

import in.ac.gpckasaragod.saloon.inventory.management.model.ui.data.ServiceDetails;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import in.ac.gpckasaragod.saloon.inventory.management.service.SaloonService;


/**
 *
 * @author student
 */
public class SaloonServiceImpl extends ConnectionServiceImpl implements  SaloonService {

    @Override
    public String saveService(Integer saloonId, String serviceName, String price) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
       try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO SERVICE(SALOON_ID ,SERVICE_NAME,PRICE) VALUES " +"("+saloonId+",'"+serviceName+"',"+price+")";
            System.out.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status!=1){
                return "Saved Failed";
            }
            else{
                return "Saved Successfully";
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(SaloonServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            return "Saved Failed";
        } 
    }

    @Override
    public ServiceDetails readService(Integer Id) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        ServiceDetails service =null;
        try{
            Connection connection =getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM  SERVICE WHERE ID="+Id;
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                int id = resultSet.getInt("ID");
                int saloonId = resultSet.getInt("SALOON_ID");
                String serviceName = resultSet.getString("SERVICE_NAME");
                Double price = resultSet.getDouble("PRICE");
                service = new ServiceDetails(id,saloonId,serviceName,price);
                
            }   
        } catch (SQLException ex) {
            Logger.getLogger(SaloonServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return service;
    }

    @Override
    public List<ServiceDetails> getAllServices() {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        List<ServiceDetails> services = new ArrayList<>();
    try{
        Connection connection =getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM SERVICE";
    ResultSet resultSet = statement.executeQuery(query);
    
     while(resultSet.next()){
        int id = resultSet.getInt("ID");
               int saloonId =resultSet.getInt("SALOON_ID");
               String serviceName =resultSet.getString("SERVICE_NAME");
               Double price = resultSet.getDouble("PRICE");
               ServiceDetails service = new ServiceDetails (id,saloonId,serviceName,price);
               services.add(service);
     }
        
    }   catch (SQLException ex) {
            Logger.getLogger(SaloonServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return services;
    }

    @Override
    public String updateService(Integer id, Integer saloonId, String ServiceName, String price) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        try{
            Connection connection =getConnection();
            Statement statement = connection.createStatement();
            String query = "UPDATE SERVICE SET SALOON_ID ='"+saloonId+"',SERVICE_NAME ='"+ServiceName+"',PRICE = '"+price+"' WHERE ID="+id;
            System.out.print(query);
            int update = statement.executeUpdate(query);
            if(update !=1)
            return "Update Failed";
            else
                return "Updated successfully";
            
    }   catch (SQLException ex) {
        return "Updated Failed";
            
        }
    }

    @Override
    public String deleteService(Integer id) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
         try{
            Connection connection =getConnection();
            String query = "DELETE FROM SERVICE WHERE ID=?";
            PreparedStatement statement = connection.prepareCall(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if (delete !=1)
                return "Delete Failed";
            else
                return "Deleted Successfully";
            
            
        } catch (Exception ex) {
            ex.printStackTrace();
            return "Deleted Failed";
        }
    }  
}